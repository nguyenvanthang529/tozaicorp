<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tozaicorp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{yEpKBrFawC+@3ecYHnA?:dmA#&.(L_%G+ibZL1PT^ITTL2?i3_.eIsSa-.s^hI(');
define('SECURE_AUTH_KEY',  '+r+U/`E7*Q3@YzH?*rx07$|(?B ~.ZVG@zG`uVmsW~et?Rq<-;FoJ>_&|7L)x5@~');
define('LOGGED_IN_KEY',    '+:8V_{#zBQDNh.j 3P< #G.,?ZlG0v..tXVK>o<d!b&mRJCX5Y_N83Hr9<PM<%Eg');
define('NONCE_KEY',        'm}v*Qro~@G6cllWKbzNud1-ZXfGWc9Z@7k{c.pB;@ebd=bV+Cr<,?OUrAxl KMRL');
define('AUTH_SALT',        'S)@tM;s;DnCK+S9[8KDFJ[MzDp&]sl+l!Qvi.<}580&vlM9yWc`/k OCp$,U(/YE');
define('SECURE_AUTH_SALT', '`,^joUydIs,fqtYvea]3_%j2vY*ZP+AXCF9t<g)Vn*iq` %|v3UagW:V=VYl6[|`');
define('LOGGED_IN_SALT',   '(|;&f5OP%!|#GY|+AkkCZe~r9U0+[NVD7Bh]Y3g#Elp5A9EI)b%d5vZ,d(Q0hsoW');
define('NONCE_SALT',       'wwf)]Dgt({)k9w_HfN;3~s$f^+&_8:6QuCi-Nn`<($~2@?fG_x&I]@e[p].e7 4X');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
