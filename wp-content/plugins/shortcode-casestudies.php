<?php

/**
 * Plugin Name: Shortcode casestudies
 * Plugin Author: Zoam
 */

function create_shortcode_casestudies() {
  
   $query = array(
      'post_type' => 'case_studies',
  );
  
  $loop = new WP_Query($query);

  $loop->posts = array_reverse($loop->posts);

   ob_start();
        if ( $loop->have_posts() ) :?>
               <!-- Swiper -->
                <div class="swiper-container swiper-container-casestudies swiper_casestudies-height">
                  <div class="swiper-wrapper">

                <?php
                while ( $loop->have_posts() ) :
                  
                        $loop->the_post();?>
                
                        <div class="swiper-slide ">
                           <div class="slideCasestudies">
                                <p class="slideCasestudies_title">
                                    <?= the_field('title');?>
                                </p>
                                <p class="slideCasestudies_name">
                                    <?= the_field('name');?>
                                </p>
                                <p class="slideCasestudies_content">
                                    <?= the_field('content');?>
                                </p>
                                <a href="<?= the_permalink(); ?>" class="slideCasestudies_link">
                                    Read more &nbsp; &rarr;
                                </a>
                           </div>
                        </div>

                <?php endwhile; ?>


                  </div>
                  
                  
               </div>
               
               <?php
        endif;
        $list_post = ob_get_contents(); // Lấy toàn bộ nội dung ở phía trên bỏ vào biến $list_post để return.
  
        ob_end_clean();
  
        return $list_post;
}
add_shortcode('shortcode_casestudies', 'create_shortcode_casestudies');
