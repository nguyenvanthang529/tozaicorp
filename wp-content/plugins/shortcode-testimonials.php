<?php

/**
 * Plugin Name: Shortcode testimonials
 * Plugin Author: Zoam
 */

function create_shortcode_testiminials() {
  
   $query = array(
      'post_type' => 'testimonials',
  );
  
  $loop = new WP_Query($query);

  $loop->posts = array_reverse($loop->posts);

   ob_start();
        if ( $loop->have_posts() ) :?>
               <!-- Swiper -->
               <div class="swiper-button-next swiper-next-fix swiper-button-fix" > > </div>
               <div class="swiper-button-prev swiper-prev-fix swiper-button-fix"> < </div>

                <div class="swiper-container swiper-container-testimonials">
                  <div class="swiper-wrapper swiper-wrap-fix">

                <?php
                while ( $loop->have_posts() ) :
                  
                        $loop->the_post();?>
                
                        <div class="swiper-slide swiper-slide-fix swiper_testimonials-reponsive">
                           <div class="slideTestimonial">
                              <div class="slideTestimonial_wrap">
                                    <div class="slideTestimonial_image">
                                       <img src="<?= the_field('image'); ?>">
                                    </div>
                                    <div class="slideTestimonial_text">
                                       <p class="slideTestimonial_content">
                                          <?= the_field('content');?>
                                       </p>
                                       <p class="slideTestimonial_name">
                                          <?= the_field('name');?>
                                       </p>
                                       <p class="slideTestimonial_positions">
                                          <?= the_field('position');?>
                                       </p>
                                       <p class="slideTestimonial_company">
                                          <?= the_field('company');?>
                                       </p>
                                    </div>
                              </div>
                           </div>
                        </div>

                <?php endwhile; ?>


                  </div>

                  <!-- Add Arrows -->
                  
               </div>
               
               <?php
        endif;
        $list_post = ob_get_contents(); // Lấy toàn bộ nội dung ở phía trên bỏ vào biến $list_post để return.
  
        ob_end_clean();
  
        return $list_post;
}
add_shortcode('shortcode_testiminials', 'create_shortcode_testiminials');
