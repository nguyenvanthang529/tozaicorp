<?php

/**
 * Plugin Name: Shortcode About
 * Plugin Author: Zoam
 */

function create_shortcode_about() {
  
   $query = array(
      'post_type' => 'about_images',
  );
  
  $loop = new WP_Query($query);

  $loop->posts = array_reverse($loop->posts);

   ob_start();
        if ( $loop->have_posts() ) :?>
               <!-- Swiper -->
                <div class="swiper-container swiper-container-about">
                  <div class="swiper-wrapper slider-about_fix">

                <?php
                while ( $loop->have_posts() ) :
                  
                        $loop->the_post();?>
                
                        <div class="swiper-slide ">
                           <img src="<?= the_field('image'); ?>" alt="image">
                        </div>

                <?php endwhile; ?>

                  </div>
                  
                  
               </div>
               
               <?php
        endif;
        $list_post = ob_get_contents(); // Lấy toàn bộ nội dung ở phía trên bỏ vào biến $list_post để return.
  
        ob_end_clean();
  
        return $list_post;
}
add_shortcode('shortcode_about', 'create_shortcode_about');
