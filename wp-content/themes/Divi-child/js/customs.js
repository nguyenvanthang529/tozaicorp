

var swiper = new Swiper('.swiper-container-testimonials', {
    slidesPerView: 2,
    spaceBetween: 30,
    // loop: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  var swiper = new Swiper('.swiper-container-casestudies', {
    slidesPerView: 1,
    spaceBetween: 30,
    pagination: {
      el: '.swiper-pagination-casestudies',
      type: 'custom',
      renderCustom: function(swiper, current, total) {
        return "<span class='swiper-pagination-current'>0"+ current +"</span>" + 
          "<span class='swiper-pagination-total'>0"+ total +"</span>";
      }
    },
    navigation: {
      nextEl: '.swiper-casestudies-next',
      prevEl: '.swiper-casestudies-prev',
    },
  });

  var swiper = new Swiper('.swiper-container-about', {
    slidesPerView: 1,
    spaceBetween: 100,
    // autoplay: {
    //   delay: 2000,
    //   disableOnInteraction: false,
    //   reverseDirection: false,
    // },
    navigation: {
      nextEl: '.swiper-about-next',
      prevEl: '.swiper-about-prev',
    },
  });