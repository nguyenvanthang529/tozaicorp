<?php

/**
 * Template Name: NewPage
 */

get_header();
?>

    <div class="body-content">
		<?php
		    while ( have_posts() ) : the_post(); 

			the_content();

			endwhile;?>
	</div> <!-- .body-content -->

 <?php
 get_footer();
 ?>