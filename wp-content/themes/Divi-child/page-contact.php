<?php

/**
 * Template Name: pageContact
 */

?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <link rel="icon" type="image/png"  class="icon-tozai-logo" href="<?= get_theme_file_uri() . '/images/tozai_images/icon-logo.png' ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="<?= get_theme_file_uri() . "/css/style.css" ?>">>
    <link rel="stylesheet" href="<?= get_theme_file_uri() . "/css/contact-page.css" ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

    <header id="masthead" class="site-header">
        <div class="site-header_navbar">
            <nav class="navbar navbar-default navbar-fix">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-wrap">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand navbar-brand_fix" href="/">
                                <img src="<?= get_theme_file_uri() . '/images/tozai_images/TOZAI_Logo.png'?>" title="logo_tozaicorp" alt="logo_tozaicorp">
                            </a>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <?php  wp_nav_menu( array( 'theme_location' => 'primary-menu', 'menu_id' => 'main-menu' ) ); ?>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.navbar-wrap -->
                </div><!-- /.container -->
            </nav>
        </div>
        <div class="site-header_contact">
           <?= do_shortcode('[contact-form-7 id="273" title="form contact"]')?>
        </div> <!-- /.site-header_contact -->

        <div class="site-header_content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="header-content_wrap">
                            <h1>TOZAI COPORATION</h1>
                            <p> <span style="color: #c01823"> A. </span>
                                210-0004   Kawasaki-Shi, <br>
                                Kawasaki-Ku Miyamoto-Cho 6-12  2F 201
                            </p>
                            <p> <span style="color: #c01823"> T.  </span> &#160; 070 3871 3388 <br>
                                <span style="color: #c01823"> M.  </span>  &#160; minhduyphan@touzaicorp.com  <br>
                                <span style="color: #c01823"> W.  </span>   &#160; www.tozaicorp.com
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /.site-header_content -->

    </header>  <!-- /.site-header_navbar -->

	<div id="content" class="site-content">


        <div class="body-content">
            <?php
                while ( have_posts() ) : the_post(); 

                the_content();

                endwhile;?>
        </div> <!-- .body-content -->


	</div><!-- #content -->

	<footer id="colophon">
        <div class="site-footer">
			<div class="container">
				<div class="site-footer_second">
					<div class="footer_second-wrap">
						<div class="footer_second-terms">
							<p>	Privacy Policy </p>
							<p> Terms and Conditions </p>
						</div>
						<div class="footer_second-copyright">
							<p> © 2018 Copyright, All Rights Reserved by Tozai Corp.</p>
						</div>
					</div>
				</div>
			</div>
        </div><!-- .site-footer -->
	</footer><!-- #colophon -->
</div><!-- #page -->


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<?php wp_footer(); ?>

</body>
</html>
