<?php

get_header(); ?>


<div class="caseStudies_single">

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h1 class="caseStudies_single_title">
                    Invest in yourself or no one else will
                </h1>
            </div>
            <div class="col-md-8">
                <div class="caseStudies_single_wrap">
                    <div class="caseStudies_single_image">
                        <img src="<?= the_field('image'); ?>">
                    </div>
                    <div class="caseStudies_wrap-content">
                        <p class="caseStudies_single_name">
                            <?= the_field('name'); ?>
                        </p> 
                        <p class="caseStudies_single_dateTime">
                            <?php 
                                echo get_the_date('F j, Y')
                            ?>
                        </p>
                        <p class="caseStudies_single_content">
                            <?= the_field('content'); ?>
                        </p> 
                    </div>
                    
                </div> <!-- /.caseStudies_wrap -->

            </div>  <!-- .row -->
        </div>
    </div> <!-- .container -->

</div> <!-- .CaseStudies_single -->
		
        
        <!-- add shortcode divi -->
        <?php echo do_shortcode('[et_pb_section global_module="266"][/et_pb_section]'); ?>
        <?php echo do_shortcode('[et_pb_section global_module="267"][/et_pb_section]'); ?>
        <?php echo do_shortcode('[et_pb_section global_module="245"][/et_pb_section]'); ?>
<?php 
get_footer();
