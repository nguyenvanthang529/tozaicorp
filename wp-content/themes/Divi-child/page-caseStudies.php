<?php
/**
 * Template Name: postCaseStudies
 */
$query = array(
    'post_type' => 'case_studies',
);

$loop = new WP_Query($query);

$loop->posts = array_reverse($loop->posts);


 get_header(); ?>

<div class="caseStudies">
    <div class="container">
        <div class="row">
             <div class="col-md-12">
                <div class="caseStudies_case">
                    <h1>Case Studies</h1>
                </div>
            </div>
        </div>
        <div class="row">
           
        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="col-md-4">
                <div class="caseStudies_wrap">
                    <div class="caseStudies_image">
                        <img src="<?= the_field('image'); ?>">
                    </div>
                    <div class="caseStudies_wrap-content">
                        <p class="caseStudies_datePost">
                            <?php 
                                echo get_the_date('F j, Y')
                            ?>
                        </p>
                        <p class="caseStudies_title">
                            <?= the_field('title'); ?>
                        </p>  
                        <p class="caseStudies_name">
                            <?= the_field('name'); ?>
                        </p> 
                        <p class="caseStudies_content">
                            <?= the_field('content'); ?>
                        </p> 
                        <a href="<?= the_permalink(); ?>" class="caseStudies_link">
                            Read more 	&nbsp; &rarr;
                        </a>
                    </div>
                    
                </div> <!-- /.caseStudies_wrap -->
            </div><!-- /.col-md-4 -->
            <?php endwhile; // End of the loop. ?>
        </div>
    </div>   <!-- /.container -->
</div> <!-- /.case-studies -->

<div>
    <?= do_shortcode('[contact-form-7 id="273" title="form contact"]') ?>
</div>

 <?php
 get_footer(); 