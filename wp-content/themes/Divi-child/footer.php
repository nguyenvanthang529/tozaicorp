<?php	
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _tozaicorp
 */

?>

	</div><!-- #content -->

	<footer id="colophon">
        <div class="site-footer">
			<div class="container">

				<div class="site-footer_firstPartial">
					<div class="footer_firstPartial-wrap">
						<div class="logo-footer">
							<a href="/">
								<img src="<?= get_template_directory_uri() . '/images/tozai_images/Tozai_logo-footer.png'?>" alt="logo_tozaicorp" alt="logo_tozaicorp">
							</a>
						</div>
						<div class="footer-navbar">
							<div class="footer-navbar_wrap">
								<?php  wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_id' => 'footer-menu' ) ); ?>
							</div>
						</div>
						<div class="footer-connect">
							<a href="#"> <img src="<?= get_theme_file_uri() . '/images/tozai_images/fb.png'?>" alt="facebook" title="link-facebook"></a>
							<a href="#"> <img src="<?= get_theme_file_uri() . '/images/tozai_images/tw.png'?>" alt="facebook" title="link-twitter"></a>
						</div>
					</div> 
				</div><!-- .site-footer_firstPartial-->
				<div class="site-footer_second">
					<div class="footer_second-wrap">
						<div class="footer_second-terms">
							<p>	Privacy Policy </p>
							<p> Terms and Conditions </p>
						</div>
						<div class="footer_second-copyright">
							<p> © 2018 Copyright, All Rights Reserved by Tozai Corp.</p>
						</div>
					</div>
				</div>

			</div>
        </div><!-- .site-footer -->
	</footer><!-- #colophon -->
</div><!-- #page -->


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

 <!-- Swiper JS -->
<script src="<?= get_theme_file_uri() . '/css/dist/js/swiper.min.js' ?>"></script>
<script src="<?= get_theme_file_uri() . '/js/customs.js' ?>" ></script>


<?php wp_footer(); ?>

</body>
</html>
