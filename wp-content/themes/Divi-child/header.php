<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _tozaicorp
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <link rel="icon" type="image/png"  class="icon-tozai-logo" href="<?= get_theme_file_uri() . '/images/tozai_images/icon-logo.png' ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= get_theme_file_uri() . '/css/dist/css/swiper.min.css' ?>">
    <link rel="stylesheet" href="<?= get_theme_file_uri() . '/css/style.css' ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

    <header id="masthead" class="site-header">
        <div class="site-header_navbar">
            <nav class="navbar navbar-default navbar-fix navbar-langugle-flex">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-wrap">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand navbar-brand_fix" href="/">
                                <img src="<?= get_theme_file_uri() . '/images/tozai_images/TOZAI_Logo.png'?>" title="logo_tozaicorp" alt="logo_tozaicorp">
                            </a>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <?php  wp_nav_menu( array( 'theme_location' => 'primary-menu', 'menu_id' => 'main-menu' ) ); ?>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.navbar-wrap -->
                </div><!-- /.container -->
               <div class="site_languge">
                 <?php do_action('icl_language_selector'); ?>
               </div>
            </nav>
        </div>
        <div class="site-header_second">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="header-second_wrap">
                            <h1 class="header-second_title"> Making more <span style="color: #c01823">Possible </span></h1>
                            <p class="header-second_text">At times, you will have to step out of your comfort zone to realize significant gains. Know the boundaries of your comfort zone and practice stepping out of it in small doses.</p>
                            <a href="/case-studies-single" class="header-second_link">
                                Read more
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>  <!-- /.site-header_navbar -->

	<div id="content" class="site-content">
